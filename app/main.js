var express = require('express')
	, app = [];
app['app'] = express();
app['app'].set('views', __dirname + '/view');
app['app'].set('view engine', 'jade');
app['routes'] = require('../config/routes')(app.app);

module.exports = app